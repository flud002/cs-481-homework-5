﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Homework5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
       
        public MainPage()
        {
            this.BackgroundColor = Color.Blue;
            InitializeComponent();
            var initialLocation = MapSpan.FromCenterAndRadius(new Position(33.1307785, -117.1601826),Distance.FromMiles(1));
            MyMap.MoveToRegion(initialLocation);
            PlaceMarker();
        }
        
        void Type_Switch(System.Object sender,System.EventArgs e)
        {
            
            var button = (Button)sender;
            var type = button.Text;
            if(type=="Satellite View")
            {
                MyMap.MapType = MapType.Satellite;
            }
            if (type == "Street View")
            {
                MyMap.MapType = MapType.Street;
            }
            if (type == "Hybrid View")
            {
                MyMap.MapType = MapType.Hybrid;
            }
            
        }
        private void PlaceMarker()
        {
            var position1 = new Position(33.129224, -117.159721);
            var position2 = new Position(33.130698, -117.159420);
            var position3 = new Position(33.128155, -117.157865);
            var position4 = new Position(33.126277, -117.156985);
            var pin1 = new Pin
            {
                Type = PinType.Place,
                Position = position1,
                Label = "Kellog Library",
                Address = "This is the library on campus."
            };
            var pin2 = new Pin
            {
                Type = PinType.Place,
                Position = position2,
                Label = "Panda Express",
                Address = "This is the Panda Express on Campus"
            };
            var pin3 = new Pin
            {
                Type = PinType.Place,
                Position = position3,
                Label = "Markstein Hall",
                Address = "This is a Building on Campus"
            };
            var pin4 = new Pin
            {
                Type = PinType.Place,
                Position = position4,
                Label = "Parking Lot F",
                Address = "This is a Parking Lot on Campus"
            };
            MyMap.Pins.Add(pin1);
            MyMap.Pins.Add(pin2);
            MyMap.Pins.Add(pin3);
            MyMap.Pins.Add(pin4);
        }
        }
 }

